#!/usr/bin/env node

const path = require('path')
const spawn = require('child_process').spawn

const parseArgs = require('minimist')
const sh = require("shelljs")


const argv = parseArgs(process.argv.slice(2), {
  alias: {
    h: 'help',
    m: 'modern',
    d: 'debug'
  },
  boolean: ['h', 'm', 'd']
})

function helpAndExit() {
  console.log(`
    Description
      Runs nuxt commands with presets appropriate for nuxt-thematic builds
    Usage
      $ nuxt-thematic <preview|dist>
    Options
      --modern, -m         Target modern browsers only
      --debug, -d          Activate the Node.js debugger
  `)
  process.exit(0)
}

if (argv.help) {
  helpAndExit()
}

const action = process.argv[2]

const nuxtConfigPath = path.resolve(__dirname, '..', 'lib', 'nuxt.config.js')
const nuxtPath = path.resolve('node_modules', 'nuxt', 'bin', 'nuxt')

let nuxt_mode = 'dev'

if (action === 'preview') {
  nuxt_mode = 'dev'
}
else if (action === 'dist') {
  nuxt_mode = 'generate'
}
else {
  helpAndExit()
}

let cmd_args = [nuxtPath, nuxt_mode, '-c', nuxtConfigPath]

if (argv['debug']) {
  cmd_args.unshift('inspect')
}

let executable = 'node'

if (argv['modern']) {
  executable = path.resolve('node_modules', '.bin', 'cross-env')

  if (argv['debug']) {
    cmd_args.unshift('node')
  }

  cmd_args.unshift('BROWSER_TARGET=modern')
}

// console.log(executable, cmd_args)

let cmd = spawn(executable, cmd_args, { stdio: 'inherit' })
cmd.on('exit', function(code){
  process.exit(code)
})
