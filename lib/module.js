const fs = require('fs')
const http = require('http')
const path = require('path')
const url = require('url');
const util = require('util')

const _ = require('lodash');
const chokidar = require('chokidar')
const debug = require('debug')('nuxt:build')
const glob = require('glob')
const hjson = require('hjson')
const mkdirp = require('mkdirp')
const webpack = require('webpack')


// NOTE: Set wait to a low value to mitigate a race condition
//  where a file is removed, triggering nuxt's built-in watch
//  which causes webpack to error when it finds out the file is
//  missing. As a bonus, the user sees more immediate feedback on
//  their change.
const REFRESH_FILES_DEBOUNCE_MS = 10 // Nuxt default is 200
const PAGE_OPTIONS_SERVER_PORT = 8709

const THEME_DIR = 'theme'
const BABEL_CONFIG_FILENAME = 'babel.config.js'
const VUE_CONFIG_FILENAME = 'vue.config.js'
const NUXT_CONFIG_FILENAME = 'nuxt.config.js'
const NAV_MODULE_FILENAME = 'nav.js'


module.exports.meta = require('../package.json')


module.exports = function ThematicModule () {
  // TODO: Error handling for missing files, and missing
  //  pages attribute, or pages attribute that is not an array, or is missing
  //  required attributes for each item, etc.

  const templatesDir = path.resolve(this.options.srcDir, 'templates')

  const styleOptionsDir = path.resolve(this.options.buildDir, 'theme', 'style')
  const sitePath = path.resolve(this.options.buildDir, 'theme', 'site')

  const vueConfigPathDefault = path.resolve(__dirname, VUE_CONFIG_FILENAME)
  const vueConfigPathUser = path.resolve(this.options.srcDir, THEME_DIR, VUE_CONFIG_FILENAME)

  const dataPath = path.resolve(this.options.srcDir, THEME_DIR, 'data.hjson')
  const configPath = path.resolve(this.options.srcDir, THEME_DIR, 'options.hjson')

  // -- Cache original values that we will override later --

  this.options._nuxtHead = this.options.head

  // -- Set up vendor libs, Vue plugins, mixins, etc. --

  this.options.plugins.push(vueConfigPathDefault)

  if (fs.existsSync(vueConfigPathUser)) {
    this.options.build.watch.push(vueConfigPathUser)
    this.options.plugins.push(vueConfigPathUser)
  }

  loadBabelConfig(BABEL_CONFIG_FILENAME, this.options)
  loadNuxtConfigUser(NUXT_CONFIG_FILENAME, this.options)

  configureWebpackAliases(this.options, sitePath)

  // -- Global style auto-discovery --

  const styleDir = path.resolve(this.options.srcDir, 'assets', 'css')
  this.options._configuredCSS = this.options.css

  Object.defineProperty(this.options, 'css', {
    get: () => {
      const styleDirGlob = path.join(styleDir, '[^_]*')
      const discoveredCSS = glob.sync(styleDirGlob)
      return this.options._configuredCSS.concat(discoveredCSS)
    }
  })

  // -- Global style variables --

  configureStyleLoaders(this.options, styleOptionsDir)

  // -- Templated pages --

  this.options.build.createRoutes = (srcDir) => {

    // -- Compose routes --

    // NOTE: Load the data each time, in case it's changed since the last render
    const themeData = loadThemeData(dataPath)

    // TODO: Catch errors such as missing
    //  page properties, and log helpful messages.
    const routes = themeData.pages.map(page => {
      const component = path.resolve(templatesDir, page.template + '.vue')

      // NOTE: Normalize the path rather than blowing up when
      //  the user makes a typo or is unsure about what is expected.
      if (!page.path.startsWith('/')) {
        page.path = '/' + page.path
      }

      pageName = page.name || page.path.slice(1).replace('/', '--')

      const route = {
        name: pageName,
        component,
        path: page.path
      }

      return route
    })

    // -- Return generated routes --

    return routes
  }

  this.nuxt.plugin('build', builder => {

    // -- Page option data server --

    const pageOptionServer = http.createServer((req, resp) => {
      const parsedURL = url.parse(req.url)
      const path = parsedURL.pathname

      // NOTE: Load the data each time, in case it's changed
      const themeData = loadThemeData(dataPath)

      const page = themeData.pages.find(p => p.path === path)
      if (page === undefined) {
        resp.statusCode = 404
        resp.end()
      }
      else {
        const pageOptions = Object.assign(
          // Include head metadata as a page option
          {
            head: page.head || {}
          },

          // Merge global and per-page options
          themeData.pageOptions,
          page.pageOptions
        )

        resp.setHeader('Content-Type', 'application/json')
        resp.end(JSON.stringify(pageOptions))
      }
    })

    pageOptionServer.listen(PAGE_OPTIONS_SERVER_PORT)

    this.nuxt.plugin('close', () => {
      pageOptionServer.close()
    })

    // -- Custom watches --

    const watchList = [dataPath, configPath, styleDir, templatesDir]

    const watchOptions = Object.assign({}, this.options.watchers.chokidar, {
      ignoreInitial: true
    })

    // NOTE: We can't simply rely on this.options.build.watch because it
    //  only watches for the 'change' event on those files.
    const refreshFiles = _.debounce(() => builder.generateRoutesAndFiles(), REFRESH_FILES_DEBOUNCE_MS)
    let customFilesWatcher = chokidar.watch(watchList, watchOptions)
      .on('add', refreshFiles)
      .on('unlink', refreshFiles)
      .on('change', refreshFiles)

    this.nuxt.plugin('close', () => {
      customFilesWatcher.close()
    })

    builder.plugin('generate', async (builder, templatesFiles, templateVars) => {

      // -- Configure vue plugins, head default --

      const themeConfig = loadThemeConfig(configPath)

      // TODO: Why doesn't this work?
      // this.options.head = Object.assign({}, this.options._nuxtHead, themeConfig.head)

      // NOTE: This isn't as foolproof as the above, but should work well
      //  enough for now.
      Object.assign(this.options.head, this.options._nuxtHead, themeConfig.head)

      // -- Generate style option globals --

      const themeData = loadThemeData(dataPath)
      generateStyleOptions(themeData.styleOptions, styleOptionsDir)

      // -- Generate/set global site data --

      process.env.SITE_DOMAIN = themeData.domain

      generateNavModule(this.options, dataPath, sitePath)

    })
  })
}


function loadBabelConfig(filename, options) {
  const babelConfigPath = path.resolve(options.srcDir, THEME_DIR, filename)

  let babelConfig = {}

  try {
    babelConfig = require(babelConfigPath)
  }
  catch (e) {
    if (e instanceof SyntaxError) {
      debug('Syntax error while parsing %s; ignoring extra Babel options.', filename)
    }
    else {
      // NOOP: File is missing; ignore
    }
  }

  if (babelConfig.plugins) {
    options.build.babel.plugins = _.uniq(options.build.babel.plugins.concat(babelConfig.plugins))
  }
}


function loadNuxtConfigUser(filename, options) {
  const nuxtConfigPath = path.resolve(options.srcDir, THEME_DIR, filename)

  let webpackConfig = {}

  try {
    nuxtConfig = require(nuxtConfigPath)
  }
  catch (e) {
    if (e instanceof SyntaxError) {
      debug('Syntax error while parsing %s; ignoring extra webpack options.', filename)
    }
    else {
      // NOOP: File is missing; ignore
    }
  }

  if (nuxtConfig.build && nuxtConfig.build.vendor) {
    options.build.vendor = _.uniq(options.build.vendor.concat(nuxtConfig.build.vendor))
  }

  if (nuxtConfig.plugins) {
    options.plugins = options.plugins.concat(nuxtConfig.plugins)
  }

  if (nuxtConfig.modules) {
    options.modules = options.modules.concat(nuxtConfig.modules)
  }

  if (nuxtConfig.loading === true) {
    // NOOP: This is the default
  }
  else if (nuxtConfig.loading === false) {
    options.loading = false
  }
  else if (nuxtConfig.loading instanceof Object) {
    Object.assign(options.loading, nuxtConfig.loading)
  }
  else if (typeof nuxtConfig.loading === 'string') {
    options.loading = nuxtConfig.loading
  }
}


function styleOptionsPath(styleOptionsDir, ext) {
  return path.join(styleOptionsDir, '_options.' + ext)
}


function generateStyleOptions(styleOptions, styleOptionsDir) {
  const languages = [
    {
      ext: 'less',
      template: '@%s: %s;'
    },
    {
      ext: 'sass',
      template: '$%s: %s'
    },
    {
      ext: 'scss',
      template: '$%s: %s;'
    },
    {
      ext: 'styl',
      template: '$%s = %s;'
    },
  ]

  mkdirp.sync(styleOptionsDir)

  for (lang of languages) {
    const outputLines = Object.entries(styleOptions).map(
      ([name, value]) => util.format(lang.template, name, value)
    )

    const output = outputLines.join('\n')
    const outputPath = styleOptionsPath(styleOptionsDir, lang.ext)

    fs.writeFileSync(outputPath, output)
  }
}


function configureStyleLoaders(options, styleOptionsDir) {
  extendWebpack(options, (config, ctx) => {
    const subloaders = config.module.rules[0].options.loaders

    for (const lang of ['scss', 'sass', 'less', 'stylus', 'styl']) {
      const ext = lang.slice(0, 4)

      subloaders[lang].push({
        loader: 'sass-resources-loader',
        options: {
          resources: styleOptionsPath(styleOptionsDir, ext)
        }
      })
    }
  })
}


function generateNavModule(options, dataPath, sitePath) {
  const themeData = loadThemeData(dataPath)

  const output = 'export default ' + JSON.stringify(themeData.nav)
  const outputPath = path.join(sitePath, NAV_MODULE_FILENAME)

  mkdirp.sync(sitePath)
  fs.writeFileSync(outputPath, output)

  // Always de-dupe 'Site/*' modules to the commons chunk
  options.build.vendor.push(outputPath)
}


function configureWebpackAliases(options, sitePath) {
  extendWebpack(options, (config, ctx) => {
    Object.assign(config.resolve.alias, {
      'Site': sitePath
    })
  })
}


function loadThemeData(dataPath) {
  let themeDataRaw = '{}'

  try {
    themeDataRaw = fs.readFileSync(dataPath, 'utf8')
  }
  catch (e) {
    // NOOP: Assume the file is missing, use default
  }

  // TODO: Validate structure with JSON-schema
  // TODO: Catch exceptions and call debug()
  const themeData = hjson.parse(themeDataRaw)

  // Default to empty lists and objects
  themeData.pages = themeData.pages || []
  themeData.pageOptions = themeData.pageOptions || {}
  themeData.styleOptions = themeData.styleOptions || {}
  themeData.nav = themeData.nav || []

  return themeData
}


function loadThemeConfig(configPath) {
  // TODO: Validate structure with JSON-schema
  const themeConfig = hjson.parse(fs.readFileSync(configPath, 'utf8'))

  // Default to empty lists and objects
  themeConfig.head = themeConfig.head || {}
  themeConfig.plugins = themeConfig.plugins || []

  return themeConfig
}


function extendWebpack(options, func) {
  const otherExtend = options.build.extend

  options.build.extend = (config, ctx) => {
    otherExtend(config, ctx)
    func(config, ctx)
  }
}