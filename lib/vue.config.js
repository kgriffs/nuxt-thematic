import Vue from 'vue'
import nav from 'Site/nav.js'


// TODO: See if there is a way to only mix this into page components,
//  and/or contribute to the Nuxt project an ability to specify mixins
//  to apply only to page components.
const globals = {
  computed: {
    path () {
      if (this.$route === undefined) {
        return undefined
      }

      return this.$route.path
    },
    pathSegments () {
      if (this.$route === undefined) {
        return undefined
      }

      return this.$route.path.split('/').slice(1)
    },
  },
  head () {
    if (this.opt === undefined) {
      return {}
    }

    return this.opt.head
  }
}


if (process.server) {

  // -- These deps are not included in the final site --

  const axios = require('axios')

  // -- This code is not included in the final site --

  function asyncData (context) {
    return axios.get('http://localhost:8709' + context.route.path).then(res => {
      return { opt: res.data }
    })
  }

  globals.asyncData = asyncData

}


Vue.mixin(globals)


export default (context, inject) => {
  inject('nav', nav)
  inject('domain', process.env.SITE_DOMAIN)
}