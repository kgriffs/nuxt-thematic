# nuxt-thematic

WIP: NuxtJS module for creating data-driven themes with opinionated nuxt settings.

```bash
$ npm i -g install-peerdeps
$ install-peerdeps -d nuxt-thematic
```

```bash
npm run theme -- -h
npm run theme:dist -- -h
```